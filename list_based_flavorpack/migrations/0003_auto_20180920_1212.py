# Generated by Django 2.1.1 on 2018-09-20 12:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('list_based_flavorpack', '0002_listbasedregion_template'),
    ]

    operations = [
        migrations.AlterField(
            model_name='processingparameters',
            name='list_based_region',
            field=models.ForeignKey(help_text='A key-value pair object meant to hold parameters used for processing classes.', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='processing_parameters', to='list_based_flavorpack.ListBasedRegion', verbose_name='Processing Class Parameter'),
        ),
        migrations.AlterField(
            model_name='processingparameters',
            name='value',
            field=models.CharField(blank=True, help_text='The value part of the key-value pair.', max_length=400, null=True),
        ),
    ]
